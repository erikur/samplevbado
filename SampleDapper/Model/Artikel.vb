﻿Public Class Artikel
    Public Property ArtikelId As Integer
    Public Property Judul As String
    Public Property Isi As String
    Public Property UserId As String
    Public Property Tanggal As DateTime
    Public Overridable Property Pengguna As Pengguna
End Class
