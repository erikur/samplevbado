﻿Imports SampleDapper
Imports System.Configuration
Imports System.Data.SqlClient
Imports Dapper

Public Class PenggunaDAL
    Implements ICrud(Of Pengguna)

    Public Function GetConn() As String
        Return ConfigurationManager.ConnectionStrings("Default").ConnectionString
    End Function

    Public Sub Create(obj As Pengguna) Implements ICrud(Of Pengguna).Create
        Using conn As New SqlConnection(GetConn())
            Dim strSql = "insert into Pengguna(UserId,Username,Nama,Aturan) 
                          values(@UserId,@Username,@Nama,@Aturan)"
            Dim param = New With {.UserId = obj.UserId, .UserName = obj.Username,
                .Nama = obj.Nama, .Aturan = obj.Aturan}
            Try
                conn.Execute(strSql, param)
            Catch sqlEx As SqlException
                Throw New Exception("Error: " & sqlEx.Message)
            End Try
        End Using
    End Sub

    Public Sub Update(obj As Pengguna) Implements ICrud(Of Pengguna).Update
        Using conn As New SqlConnection(GetConn())
            Dim strSql = "update Pengguna Username=@Username,Nama=@Nama,Aturan=@Aturan) 
                          where UserId=@UserId"
            Dim param = New With {.UserId = obj.UserId, .UserName = obj.Username,
                .Nama = obj.Nama, .Aturan = obj.Aturan}
            Try
                conn.Execute(strSql, param)
            Catch sqlEx As SqlException
                Throw New Exception("Error: " & sqlEx.Message)
            End Try
        End Using
    End Sub

    Public Sub Delete(obj As Pengguna) Implements ICrud(Of Pengguna).Delete
        Using conn As New SqlConnection(GetConn())
            Dim strSql = "delete Pengguna  
                          where UserId=@UserId"
            Dim param = New With {.UserId = obj.UserId}
            Try
                conn.Execute(strSql, param)
            Catch sqlEx As SqlException
                Throw New Exception("Error: " & sqlEx.Message)
            End Try
        End Using
    End Sub

    Public Function GetAll() As List(Of Pengguna) Implements ICrud(Of Pengguna).GetAll
        Using conn As New SqlConnection(GetConn())
            Dim strSql = "select * from Pengguna order by Nama asc"
            Dim results = conn.Query(Of Pengguna)(strSql)
            Return results
        End Using
    End Function

    Public Function GetSingle(id As String) As Pengguna Implements ICrud(Of Pengguna).GetSingle
        Using conn As New SqlConnection(GetConn())
            Dim strSql = "select * from Pengguna where UserId=@UserId"
            Dim param = New With {.UserId = id}
            Dim result = conn.QuerySingle(Of Pengguna)(strSql, param)
            Return result
        End Using
    End Function
End Class
