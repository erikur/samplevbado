﻿Public Interface ICrud(Of T)
    Function GetAll() As List(Of T)
    Function GetSingle(id As String) As T
    Sub Create(obj As T)
    Sub Update(obj As T)
    Sub Delete(obj As T)
End Interface
