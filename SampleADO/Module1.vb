﻿Imports MyLibrary

Module Module1

    Sub Main()
        'Dim bil1 As Integer = 12
        'Dim bil2 As Integer = bil1
        'Console.WriteLine(bil2)
        'bil1 = 15

        'Console.WriteLine(bil2)

        'Dim mhs1 As New Mahasiswa
        'mhs1.Nim = "119988"

        'Dim mhs2 As Mahasiswa = mhs1
        'Console.WriteLine(mhs2.Nim)

        'mhs1.Nim = "998877"
        'Console.WriteLine(mhs2.Nim)

        'Dim mhs3 As New Mahasiswa
        'mhs3.Nim = "998877"

        'Console.WriteLine(mhs1 Is mhs3)
        'Console.WriteLine(mhs1.Nim = mhs3.Nim)


        Dim mhs As New Mahasiswa
        'mhs.SetNim("889977")
        'Console.WriteLine(mhs.GetNim())
        mhs.Nama = "Erick"
        Console.WriteLine(mhs.Nama)
        mhs.GetInfo()
        mhs.GetInfo(3.3)


        Dim kucing1 As New Kucing
        Dim kucing2 As New Kucing("Bimbim", "Putih", "Tebal")
        'kucing1.Nama = "Katty"
        'kucing1.Warna = "Hitam"
        'kucing1.Kumis = "Panjang"
        Console.WriteLine(kucing1.GetInfo())

        Dim kucingangola As New Angola
        kucingangola.Nama = "Pak Kumis"
        kucingangola.Warna = "Cokelat"
        kucingangola.Kumis = "Tebal"
        kucingangola.BuluTebal = True
        Console.WriteLine(kucingangola.GetInfo())

        Dim kucpersia As New Persia
        kucpersia.Nama = "Persi"

        'polymorhpism
        Dim listKucing As New List(Of Kucing)
        listKucing.Add(kucing1)
        listKucing.Add(kucingangola)
        listKucing.Add(kucpersia)

        'Dim listResult = From k In listKucing
        '                 Where k.Nama = "Joy"
        '                 Select k

        Dim listResult = listKucing.Where(Function(k) k.Nama = "Joy")



        'Dim listResult As New List(Of Kucing)
        'For Each kuc As Kucing In listKucing
        '    If kuc.Nama = "Joy" Then
        '        listResult.Add(kuc)
        '    End If
        'Next

        'If listResult.Count > 0 Then
        '    For Each kuc As Kucing In listResult
        '        Console.WriteLine($"Nama {kuc.Nama} dan Warna {kuc.Warna}")
        '    Next
        'End If

        'For Each kuc As Kucing In listKucing
        '    If TypeOf kuc Is Angola Then
        '        Dim kucangola = CType(kuc, Angola)
        '        Console.WriteLine($"Bulu tebal {kucangola.BuluTebal}")
        '    End If
        '    Console.WriteLine(kuc.Nama)
        'Next

        Dim myLib As New Helper
        Dim hasil = myLib.LuasSegitiga(12, 20)
        Console.WriteLine($"Hasilnya: {hasil}")


        Dim myGuid = Guid.NewGuid()
        Console.WriteLine(myGuid)



    End Sub

End Module

Public Class Mahasiswa
    'Private _nim As String
    'Public Function GetNim() As String
    '    Return _nim
    'End Function
    'Public Sub SetNim(value As String)
    '    If value Is Nothing Then
    '        value = "000000"
    '    End If
    '    _nim = value
    'End Sub
    Public Property Nim As String

    Private _nama As String
    Public Property Nama() As String
        Get
            Return _nama
        End Get
        Set(ByVal value As String)
            _nama = value
        End Set
    End Property

    'automatic property
    Public Property Alamat As String
    Public Function GetInfo() As String
        Return Nim & " " & Nama & " " & Alamat
    End Function

    'overloading method
    Public Function GetInfo(ipk As Double) As String
        Return GetInfo() & " " & ipk
    End Function
End Class

Public Class Kucing
    Public Sub New()
        Nama = "Joy"
        Warna = "Putih"
        Kumis = "Pendek"
    End Sub

    Public Sub New(_nama As String, _warna As String, _kumis As String)
        Nama = _nama
        Warna = _warna
        Kumis = _kumis
    End Sub

    Public Property Nama As String
    Public Property Warna As String
    Public Property Kumis As String

    Public Overridable Function GetInfo() As String
        Return $"{Nama} dan {Warna} dan {Kumis}"
    End Function

    Protected Function Hello() As String
        Return "Hello VB"
    End Function
End Class

Public Class Angola
    Inherits Kucing 'spesialisasi

    Public Property BuluTebal As Boolean
    Public Function HelloWorld() As String
        Return Hello()
    End Function

    Public Overrides Function GetInfo() As String
        Return MyBase.GetInfo() & $" Bulu {BuluTebal}"
    End Function
End Class

Public Class Persia
    Inherits Kucing
    Public Property Harga As Decimal

End Class



