﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports SampleADO

Public Class CategoryDAL
    Implements IDisposable, ICrud(Of Category)

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then

            End If
        End If
        disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        ' TODO: uncomment the following line if Finalize() is overridden above.
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    Public Function GetConn() As String
        Return ConfigurationManager.ConnectionStrings("Default").ConnectionString
    End Function

    'Public Function GetDS() As DataTable
    '    Using conn As New SqlConnection(GetConn())
    '        Dim strSql = "select * from Categories order by CategoryName asc"
    '        Dim cmd As New SqlCommand(strSql, conn)
    '        conn.Open()
    '        Dim da As New SqlDataAdapter
    '        Dim dt As New DataTable
    '        da.SelectCommand = cmd
    '        da.Fill(dt)

    '        da.Dispose()
    '        cmd.Dispose()
    '        conn.Close()

    '        Return dt
    '    End Using
    'End Function

    Public Function GetAll() As List(Of Category) Implements ICrud(Of Category).GetAll
        Dim strBuffer As New SqlConnectionStringBuilder
        Dim listCategory As New List(Of Category)
        Using conn As New SqlConnection(GetConn())
            Dim strSql = "select * from Categories order by CategoryName asc"
            Dim cmd As New SqlCommand(strSql, conn)

            conn.Open()
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read()
                    listCategory.Add(New Category With {
                                     .CategoryID = CInt(dr("CategoryID")),
                                     .CategoryName = dr("CategoryName").ToString()
                                     })
                End While
            End If
            dr.Close()
            cmd.Dispose()
            conn.Close()
        End Using
        Return listCategory
    End Function



    Public Sub Create(obj As Category) Implements ICrud(Of Category).Create
        Using conn As New SqlConnection(GetConn())
            Dim strSql = "insert into Category(CategoryName) values(@CategoryName)"
            Dim cmd As New SqlCommand(strSql, conn)
            cmd.Parameters.AddWithValue("@CategoryName", obj.CategoryName)
            Try
                conn.Open()
                cmd.ExecuteNonQuery()
            Catch sqlEx As SqlException
                Throw New Exception("Error: " & sqlEx.Message)
            Finally
                cmd.Dispose()
                conn.Close()
            End Try
        End Using
    End Sub

    Public Sub Update(obj As Category) Implements ICrud(Of Category).Update
        Using conn As New SqlConnection(GetConn())
            Dim strSql = "update Category CategoryName=@CategoryName where CategoryID=@CategoryID"
            Dim cmd As New SqlCommand(strSql, conn)
            cmd.Parameters.AddWithValue("@CategoryName", obj.CategoryName)
            cmd.Parameters.AddWithValue("@CategoryID", obj.CategoryID)

            Try
                conn.Open()
                cmd.ExecuteNonQuery()
            Catch sqlEx As SqlException
                Throw New Exception("Error: " & sqlEx.Message)
            Finally
                cmd.Dispose()
                conn.Close()
            End Try
        End Using
    End Sub

    Public Sub Delete(obj As Category) Implements ICrud(Of Category).Delete
        Using conn As New SqlConnection(GetConn())
            Dim strSql = "delete Category where CategoryID=@CategoryID"
            Dim cmd As New SqlCommand(strSql, conn)
            cmd.Parameters.AddWithValue("@CategoryID", obj.CategoryID)

            Try
                conn.Open()
                cmd.ExecuteNonQuery()
            Catch sqlEx As SqlException
                Throw New Exception("Error: " & sqlEx.Message)
            Finally
                cmd.Dispose()
                conn.Close()
            End Try
        End Using
    End Sub

    Public Sub BulkInsertCategory()
        Dim dt As New DataTable
        dt.Columns.Add("CategoryName")
        For i = 1 To 100
            dt.Rows.Add($"Kategori {i}")
        Next

        Using bulk As New SqlBulkCopy(GetConn())
            bulk.DestinationTableName = "Categories"
            bulk.WriteToServer(dt)
        End Using
    End Sub

    Public Function GetSingle(id As String) As Category Implements ICrud(Of Category).GetSingle
        Dim result As New Category
        Using conn As New SqlConnection(GetConn())
            Dim strSql = "select * from Categories where CategoryID=@CategoryID"
            Dim cmd As New SqlCommand(strSql, conn)
            cmd.Parameters.Clear()
            cmd.Parameters.AddWithValue("@CategoryID", id)

            conn.Open()
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                dr.Read()
                With result
                    .CategoryID = CInt(dr("CategoryID"))
                    .CategoryName = dr("CategoryName").ToString
                End With
            End If
            dr.Close()
            cmd.Dispose()
            conn.Close()
        End Using
        Return result
    End Function
End Class
